# from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QGridLayout, QWidget, QCheckBox, QSystemTrayIcon, QSpacerItem, QSizePolicy, QMenu, QAction, QStyle, qApp
import os
from PyQt5 import QtGui,QtWidgets
from PyQt5.QtCore import QSize

class AppIndicator(QtWidgets.QSystemTrayIcon):


    def __init__(self,parent=None):
        self.mensaje = ""
        self.basedir = ""
        self.basefile = ""

        self.basedir , self.basefile = os.path.split(os.path.abspath(__file__))
        QtWidgets.QSystemTrayIcon.__init__(self,parent)

        icon_img = os.path.join(self.basedir,'person.png')
        self.setIcon(QtGui.QIcon(icon_img))

        show_action = QtWidgets.QAction("Mostrar", self)
        hide_action = QtWidgets.QAction("Ocultar", self)
        quit_action = QtWidgets.QAction("Salir", self)
        mensaje_action = QtWidgets.QAction("Mensaje",self)
        registro_action = QtWidgets.QAction("Registro", self)

        show_action.triggered.connect(self.show)
        hide_action.triggered.connect(self.hide)
        mensaje_action.triggered.connect(self.mostrarMensaje)
        registro_action.triggered.connect(self.leerTexto)
        quit_action.triggered.connect(QtWidgets.qApp.quit)

        tray_menu = QtWidgets.QMenu()
        tray_menu.addAction(mensaje_action)
        tray_menu.addAction(show_action)
        tray_menu.addAction(registro_action)
        tray_menu.addAction(hide_action)
        tray_menu.addAction(quit_action)

        self.setContextMenu(tray_menu)
        self.show()
        
    def setMensaje(self, mensaje):
        self.mensaje = mensaje

    def leerTexto(self):
        texto, okPresionado =  QtWidgets.QInputDialog.getText(self, "Ingrese nombre:", QtWidgets.QLineEdit.Normal,"")
        if okPresionado and texto != '':
            return texto

    def mostrarMensaje(self):
        self.showMessage(
            "ARF",
            self.mensaje,
            QtWidgets.QSystemTrayIcon.Information,
            2000
        )
