from PyQt5.QtWidgets import QApplication,QMainWindow,QAction,QMenu,QSystemTrayIcon,qApp,QInputDialog,QLineEdit,QStyle
from OpenCv3 import captura as cap

class Ventana(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)
        self.setWindowTitle('ARF')
        self.mensaje=""
        self.captura = cap.Captura()

        self.tray_icon = QSystemTrayIcon(self)
        self.tray_icon.setIcon(self.style().standardIcon(QStyle.SP_ComputerIcon))

        leer_action = QAction("Leer", self)
        mensaje_action = QAction("Mensaje", self)
        registro_action = QAction("Registro", self)
        reconocer_action = QAction("Reconocer", self)
        salir_action = QAction("Salir", self)

        leer_action.triggered.connect(self.catchMensaje)
        mensaje_action.triggered.connect(self.showMensaje)
        registro_action.triggered.connect(self.catchUser)
        reconocer_action.triggered.connect(self.recognizeUser)
        salir_action.triggered.connect(qApp.quit)

        tray_menu = QMenu()
        tray_menu.addAction(leer_action)
        tray_menu.addAction(mensaje_action)
        tray_menu.addAction(registro_action)
        tray_menu.addAction(reconocer_action)
        tray_menu.addAction(salir_action)

        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.show()

    def catchUser(self):
        nombre = self.catchMensaje()
        self.captura.enrolar(nombre)

    def recognizeUser(self):
        self.captura.reconocer()

    def catchMensaje(self):
        text, okPressed = QInputDialog.getText(self, "Get text", "Your name:", QLineEdit.Normal, "")
        if okPressed and text != '':
            self.mensaje = text
            print(text)
            return text

    def getMensaje(self, mensaje):
        self.mensaje = mensaje


    def setMensaje(self):
        return self.mensaje

    def showMensaje(self):
        self.tray_icon.showMessage(
            "ARF",
            self.mensaje,
            QSystemTrayIcon.Information,
            2000
        )

    def identificar(self):
        nombre =self.captura.identificar()
        self.getMensaje(str(nombre))
        self.showMensaje()