import cv2
import os
import time
import numpy
import sys


class Captura():

    def __init__(self):
        self.dir_faces = 'Capturas'
        self.size = 4
        (self.images, self.lables, self.names, self.id) = ([], [], {}, 0)

        #   CARGAR INFORMACION Y PREPARAR EL MODELO

        # Crear una lista de imagenes y una lista de nombres correspondientes
        for (subdirs, dirs, files) in os.walk(self.dir_faces):
            for subdir in dirs:
                self.names[self.id] = subdir
                subjectpath = os.path.join(self.dir_faces, subdir)
                for filename in os.listdir(subjectpath):
                    path = subjectpath + '/' + filename
                    label = self.id
                    self.images.append(cv2.imread(path, 0))
                    self.lables.append(int(label))
                self.id += 1
        (self.im_width, self.im_height) = (112, 92)

        # Crear una matriz Numpy de las dos listas anteriores
        (self.images, self.lables) = [numpy.array(lis) for lis in [self.images, self.lables]]
        # OpenCv entrena un modelo a partir de las imagenes
        self.model = cv2.face.LBPHFaceRecognizer_create()
        self.model.train(self.images, self.lables)

    def enrolar(self, nombre):
        path = os.path.join(self.dir_faces, nombre)

        # Si no existe la carpeta se crea
        if not os.path.isdir(path):
            os.mkdir(path)

        # Cargar la plantilla e inicializar la webcam
        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        cap = cv2.VideoCapture(0)

        img_width, img_height = 112, 92

        #Ciclo para tomar fotografias
        count = 0
        while count < 100:
            # Leer un frame y almacenarlo
            rval, img = cap.read()
            img = cv2.flip(img, 1, 0)

            #Convertir la imagen a blanco y negro
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            #Redimensionar la imagen
            mini = cv2.resize(gray, (int(gray.shape[1]/self.size), int(gray.shape[0]/self.size)))

            # Buscar las coordenadas de los rostros y guardar la posicion
            faces = face_cascade.detectMultiScale(mini)
            faces = sorted(faces, key=lambda x:x[3])

            if faces:
                face_i = faces[0]
                (x,y,w,h) = [v * self.size for v in face_i]
                face = gray[y:y+h, x:x+w]
                face_resize = cv2.resize(face, (img_width, img_height))
                #Dibujamos un rectangulo en las coordenadas del rostro
                cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0),3)
                #Poner el nombre en la imagen
                cv2.putText(img, nombre,  (x-10,y-10), cv2.FONT_HERSHEY_PLAIN,1,(0,255,0))
                #El nombre de la foto es el numero del ciclo
                pin=sorted([int(n[:n.find('.')]) for n in os.listdir(path)
                            if n[0]!='.']+[0])[-1]+1
                #Guardar la foto en el directorio
                cv2.imwrite('%s/%s.png' % (path,pin), face_resize)

                count += 1

    def reconocer(self):
        #size = 4

        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        cap = cv2.VideoCapture(0)

        while True:
            #leer un frame y almacenarlo
            rval, frame = cap.read()
            frame=cv2.flip(frame,1,0)

            #Convertir la imagen a blanco y negro
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            #redimensionar la imagen
            mini = cv2.resize(gray, (int(gray.shape[1]/self.size), int(gray.shape[0]/self.size)))

            #buscar las coordenadas de los rostros
            faces = face_cascade.detectMultiScale(mini)

            for i in range(len(faces)):
                face_i = faces[i]
                (x,y,w,h) = [v*self.size for v in face_i]
                face = gray[y:y+h,x:x+w]
                face_resize = cv2.resize(face, (self.im_width, self.im_height))

                #Intentar reconocer la cara
                prediction = self.model.predict(face_resize)

                #Dibujar un rectangulo
                cv2.rectangle(frame, (x,y),(x+w,y+h),(0,255,0),3)

                cara = '%s' % (self.names[prediction[0]])

                if prediction[1] <100 :
                    cv2.putText(frame,'%s - %.0f' % (cara,prediction[1]),(x-10,y-10), cv2.FONT_HERSHEY_PLAIN,1,(0,255,0))
                elif prediction[1]>101 and prediction[1]<500:
                    cv2.putText(frame, 'Desconocido',(x-10,y-10), cv2.FONT_HERSHEY_PLAIN,1,(0,255,0))

                cv2.imshow('OpenCv reconocimiento facial',frame)
            break

    def identificar(self):
        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        cap = cv2.VideoCapture(0)

        # leer un frame y almacenarlo
        rval, frame = cap.read()
        frame = cv2.flip(frame,1,0)

        # Convertir la imagen a blanco y negro
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # redimensionar la imagen
        mini = cv2.resize(gray, (int(gray.shape[1] / self.size), int(gray.shape[0] / self.size)))

        # buscar las coordenadas de los rostros
        faces = face_cascade.detectMultiScale(mini)
        nombre = ''

        for i in range(len(faces)):
            face_i = faces[i]
            (x, y, w, h) = [v * self.size for v in face_i]
            face = gray[y:y + h, x:x + w]
            face_resize = cv2.resize(face, (self.im_width, self.im_height))

            # Intentar reconocer la cara
            prediction = self.model.predict(face_resize)

            # Dibujar un rectangulo
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 3)

            nombre = '%s' % (self.names[prediction[0]])
            if prediction[1] < 100:
                cv2.putText(frame, '%s - %.0f' % (nombre, prediction[1]), (x - 10, y - 10), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))
            elif prediction[1] > 101 and prediction[1] < 500:
                cv2.putText(frame, 'Desconocido', (x - 10, y - 10), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))

            # Guardar Imagen
            imageName = str(time.strftime(self.dir_faces+"/%Y_%m_%d_%H_%M_%S")) + '.jpg'
            cv2.imwrite(imageName, frame)
        cap.release()
        print(nombre)
        return nombre

    def Foto(self,usuario=""):
        cap = cv2.VideoCapture(0)
        ret, frame = cap.read()
        #formar direccion
        dir = "Capturas/"
        if len(usuario) > 0:
            dir = dir + usuario+'/'
        #Comprobar que existe directorio
        try:
            os.stat(dir)
        except:
            os.mkdir(dir)

        #Guardar Imagen
        imageName = str(time.strftime(dir+"%Y_%m_%d_%H_%M_%S")) +'.jpg'
        cv2.imwrite(imageName, frame)
        #cv2.imshow('Imagen', frame)
        cap.release()
        #time.sleep(60)

    def Mensaje(self):
        return "Mensaje desde captura.py"


    def MensajeBienvenida(self):
        return "Bienvenido ..."

    def Ciclo(self):
        while True:
            self.Foto()
