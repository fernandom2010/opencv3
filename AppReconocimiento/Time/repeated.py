import cv2
import time
from threading import Timer

class Repeated(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.function = function
        self.interval = interval
        self.args = args
        self.kwargs = kwargs
        self.is_runing = False
        self.start()

    def _run(self):
        self.is_runing = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_runing:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_runing = True

    def stop(self):
        self._timer.cancel()
        self.is_runing = False