import cv2

if __name__ == '__main__':
    video = cv2.VideoCapture(0)
    while(True):
        ret, frame = video.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        cv2.imshow('Video', frame)
        cv2.imshow('Gris', gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    video.release()
    cv2.destroyAllWindows()