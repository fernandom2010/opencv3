import cv2

if __name__ == '__main__':
    cap = cv2.VideoCapture(0)

    #Definir el codec y crea un objeto de grabacion del video
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter('salida.avi', fourcc, 20.0, (640,480))

    while(cap.isOpened()):
        ret, frame = cap.read()

        if ret == True:
            #frame = cv2.flip(frame,0)
            out.write(frame)
            cv2.imshow('Video', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    cap.release()
    cv2.destroyAllWindows()