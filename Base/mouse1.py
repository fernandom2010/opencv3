import cv2
import numpy as np

drawing = False
mode = True
ix,iy = -1,-1

def draw_circle(event, x, y, flags, param):
    global ix,iy,drawing,mode

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            if mode == True:
                cv2.rectangle(img, (ix, iy), (x,y),(0,255,0), -1)
            else:
                cv2.circle(img, (x,y), 5, (0,0,255), -1)
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode == True:
            cv2.rectangle(img, (ix,iy),(x,y),(0,255,0), -1)
        else:
            cv2.circle(img,(x,y),5,(0,0,255),-1)

if __name__ == '__main__':
    img =  np.zeros((512, 512, 3), np.uint8)
    cv2.namedWindow('imagen')
    cv2.setMouseCallback('imagen', draw_circle)

    while(True):
        cv2.imshow('imagen', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()