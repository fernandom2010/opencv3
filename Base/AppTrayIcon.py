from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QGridLayout, QWidget, QCheckBox, QSystemTrayIcon, QSpacerItem, QSizePolicy, QMenu, QAction, QStyle, qApp, QInputDialog, QLineEdit
from PyQt5.QtCore import QSize

class MiVentana(QMainWindow):

    check_box = None

    def __init__(self):
        QMainWindow.__init__(self)

        self.setWindowTitle('Mi ventana')

        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)

        grid_layout = QGridLayout(self)
        central_widget.setLayout(grid_layout)

        self.check_box = QCheckBox("Check Salir")
        grid_layout.addWidget(self.check_box,0,0)
        grid_layout.addItem(QSpacerItem(0,0,QSizePolicy.Expanding, QSizePolicy.Expanding),1,0)

        self.tray_icon = QSystemTrayIcon(self)
        self.tray_icon.setIcon(self.style().standardIcon(QStyle.SP_ComputerIcon))

        show_action = QAction("Mostrar", self)
        hide_action = QAction("Ocultar", self)
        leer_action = QAction("Leer", self)
        quit_action = QAction("Salir", self)
        mensaje_action = QAction("Mensaje",self)

        show_action.triggered.connect(self.show)
        hide_action.triggered.connect(self.hide)
        leer_action.triggered.connect(self.getText)
        mensaje_action.triggered.connect(self.mensaje)
        quit_action.triggered.connect(qApp.quit)

        tray_menu = QMenu()
        tray_menu.addAction(mensaje_action)
        tray_menu.addAction(leer_action)
        tray_menu.addAction(show_action)
        tray_menu.addAction(hide_action)
        tray_menu.addAction(quit_action)


        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.show()

    def closeEvent(self, event):
        if self.check_box.isChecked():
            self.close()
        else:
            event.ignore()
            self.hide()
            
    def getText(self):
        text, okPressed = QInputDialog.getText(self, "Get text","Your name:", QLineEdit.Normal, "")
        if okPressed and text != '':
            print(text)

    def mensaje(self):
        self.tray_icon.showMessage(
            "FM",
            "Mensaje ... ",
            QSystemTrayIcon.Information,
            2000
        )



if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    mv = MiVentana()
    mv.hide()
    mv.mensaje()
    sys.exit(app.exec())
