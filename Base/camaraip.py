import cv2

schema = '192.168.1.'
stream = 'videofeed'

host = schema+'140'
cap = cv2.VideoCapture('http://'+host+':8080/'+stream)

while True:
    ret, frame = cap.read()

    #cv2.imshow(('Camara'+str(host)), frame)
    cv2.imshow('Camara '+str(host), frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        cap.release()
        break

cv2.destroyAllWindows()